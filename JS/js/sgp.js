// 无搜索功能
const request_timeout = 5000;
const UA = 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1';
const key = '水果派';
const HOST = 'https://api.ririhair.com';

/*** js自封装的方法 ***/

/**
 * 获取链接的host(带http协议的完整链接)
 * @param url 任意一个正常完整的Url,自动提取根
 * @returns {string}
 */
function getHome(url){
    if(!url){
        return ''
    }
    let tmp = url.split('//');
    url = tmp[0] + '//' + tmp[1].split('/')[0];
    try {
        url = decodeURIComponent(url);
    }catch (e) {}
    return url
}

const http = function (url, options = {}) {
    if(options.method ==='POST' && options.data){
        options.body = JSON.stringify(options.data);
        options.headers = Object.assign({'content-type':'application/json'}, options.headers);
    }
    options.timeout = request_timeout;
    if(!options.headers){
        options.headers = {};
    }
    let keys = Object.keys(options.headers).map(it=>it.toLowerCase());
    if(!keys.includes('referer')){
        options.headers['Referer'] = getHome(url);
    }
    if(!keys.includes('user-agent')){
        options.headers['User-Agent'] = UA;
    }
    console.log(JSON.stringify(options.headers));
    try {
        const res = req(url, options);
        // if(options.headers['Authorization']){
        //  console.log(res.content);
        // }
        res.json = () => res&&res.content ? JSON.parse(res.content) : null;
        res.text = () => res&&res.content ? res.content:'';
        return res
    }catch (e) {
        return {
            json() {
                return null
            }, text() {
                return ''
            }
        }
    }
};
["get", "post"].forEach(method => {
    http[method] = function (url, options = {}) {
        return http(url, Object.assign(options, {method: method.toUpperCase()}));
    }
});

function init(cfg) {

}

function home(filter) {
    const classes = [{ type_id: 1, type_name: '视频' }];
    const filterObj = {};
    return JSON.stringify({
      class: classes,
      filters: filterObj,
    });
}

function homeVod() {
    return '{}'
}

function category(tid, pg, filter, extend) {
    if (pg <= 0 || typeof pg == 'undefined') pg = 1;
    const link = HOST + "/shuiguopai/list/1/" + pg;
    const html = http.get(link).json();
    const jo = html.data.data;
    const videos = [];
    jo.list.forEach((it) => {
      videos.push({
        vod_id: it.library_id,
        vod_name: it.title,
        vod_pic: it.img_url,
        vod_remarks: it.subtitle,
      });
    });
    //const pgCount = pg * 10 > jo.total ? parseInt(pg) : parseInt(pg) + 1;
    return JSON.stringify({
        page: parseInt(pg),
        pagecount: parseInt(jo.totalPage),
        limit: 10,
        total: parseInt(jo.totalPage) * 10,
        list: videos,
    })
}

function detail(id) {
    const vod = {
        vod_id: id,
        vod_remarks: '',
    };
    const link = HOST + "/shuiguopai/detail/" +id;
    const html = http.get(link).json();
    const jo = html.data.data;
    const url = jo.detail[1].url;
    const playlist = ['点击播放' + '$' + url];
    vod.vod_name = jo.title;
    vod.vod_content = jo.subtitle;
    vod.vod_play_from = key;
    vod.vod_play_url = playlist.join('#');
    return JSON.stringify({
        list: [vod],
    });
}

function play(flag, id, flags) {
    return JSON.stringify({
        parse: 0,
        url: id,
    });
}

function search(wd, quick) {
    const link = HOST + "/shuiguopai/" + wd;
    const html = http.get(link).json();
    const jo = html.data.data;
    const videos = [];
    jo.list.forEach((it) => {
      videos.push({
        vod_id: it.library_id,
        vod_name: it.title,
        vod_pic: it.img_url,
        vod_remarks: it.love_number,
      });
    });
    //const pgCount = pg * 10 > jo.total ? parseInt(pg) : parseInt(pg) + 1;
    return JSON.stringify({
        limit: 50,
        list: videos,
    })
}

// 导出函数对象
export default {
    init: init,
    home: home,
    homeVod: homeVod,
    category: category,
    detail: detail,
    play: play,
    search: search
}